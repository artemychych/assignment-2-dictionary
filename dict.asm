extern string_equals

section .text

global find_word
find_word:
	.loop:
		cmp rsi, 0
		je .error
		
		push rdi
		push rsi
		
		add rsi, 8
		
		call string_equals
		
		pop rsi
		pop rdi
		
		cmp rax, 0
		jne .end
		
		mov rsi, [rsi]
		jmp .loop
	.end:
		mov rax, rsi
		ret
	.error:
		xor rax, rax
		ret
