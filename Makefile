%.o: %.asm
	nasm -f elf64 -o $@ $^

main:	main.o dict.o lib.o
			ld -o $@ $^
.PHONY: clean 

build: main

clean: 
	rm -f *.o
	rm -f main

run: main
	./main

test: main
	python3 ./test.py
