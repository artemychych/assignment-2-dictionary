%include "words.inc"

extern read_word						 
extern print_newline						
extern print_string						 
extern string_length						 
extern find_word						 
extern print_error						
extern exit


%define BUFFER 256

global _start

section .data
	notfound: db "Word not found", 0
	toomuch: db "Not enough space in BUFFER", 0
	

section .text

_start:

	sub rsp, BUFFER
	mov rdi, rsp
	mov rsi, BUFFER
	call read_word
	test rax, rax
	je .first_error
	
	mov rdi, rsp
	mov rsi, next
	call find_word
	add rsp, BUFFER
	
	test rax, rax
	je .second_error
	
	add rax, 8
	mov rdi, rax
	push rax
	call string_length
	pop rdi
	add rdi, rax
	inc rdi
	call print_string
	call print_newline
	
	mov rdi, 0
	call exit

.first_error:
	mov rdi, toomuch
	call print_error
	jmp .done

.second_error:
	mov rdi, notfound
	call print_error
	

.done:
	call print_newline
	mov rdi, 1
	call exit
	
	
	
