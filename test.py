import subprocess

def br():
    print("+++++++++++++++++++++--------------+++++++++++++++++++++++")
    
tests = [
    ("first", "FIRST", "", 0),
    ("home", "", "Word not found", 0),
    ("second", "SECOND", "", 0), ("RxbLn5r9vYiWUdIuLaXcIuNygNWm6yhzjW3ADwK3n0b9SuEKm6dVEfEYhqSX5Isz3CPXj8J54QzMH0QKB1Ly8O5SzJOnHfiyo1lK0gGLusOgRd1ZwmhzWmDCoJXOLxSXRxbLn5r9vYiWUdIuLaXcIuNygNWm6yhzjW3ADwK3n0b9SuEKm6dVEfEYhqSX5Isz3CPXj8J54QzMH0QKB1Ly8O5SzJOnHfiyo1lK0gGLusOgRd1ZwmhzWmDCoJXOLxSX", "", "Not enough space in BUFFER", 0),
    ("for", "", "Word not found", 0)
]

print("Tests: ")

fail = 0
succ = 0

for i, t in enumerate(tests):
    br()
    
    cmd = ["./main"]
    res = subprocess.run(cmd, input=t[0], capture_output=True, text=True)
    
    out = res.stdout.strip()
    err = res.stderr.strip()
   
    failed = False
    
    if out != t[1]:
        failed = True
        print(f"stdout: expect = '{t[1]}', returned = '{out}'")
        
    if err != t[2]:
        failed = True
        print(f"stderr: expect = '{t[2]}', returned = '{err}'")
    
    
    if failed:
        fail += 1
        print(f"Test {i + 1} failed")
    else:
        succ += 1
        print(f"Test {i + 1} success")
br()

if fail > 0:
    print("Tests failed")  
    exit(1)
else:
    print("ALL TESTS PASSED")
    exit(0)

    

